# fix the capitalization in the following poem and change all
# occurrences of "green" to "green"
# hint: use the edit menu to convert the text to all lower-case,
#       then use multi-select to
#          convert "i" to "i" where appropriate and all "Sam" to "Sam"
#          and all "green" to "green"
I am Sam. I am Sam. Sam I am.
that Sam-i-am! that Sam-i-am! I do not like that Sam-i-am!
do would you like green eggs and ham?
I do not like them, Sam-i-am.
I do not like green eggs and ham.
would you like them here or there?
I would not like them here or there.
I would not like them anywhere.
I do not like green eggs and ham.
I do not like them, Sam-i-am.
would you like them in a house?
would you like then with a mouse?
I do not like them in a house.
I do not like them with a mouse.
I do not like them here or there.
I do not like them anywhere.
I do not like green eggs and ham.
I do not like them, Sam-i-am.
