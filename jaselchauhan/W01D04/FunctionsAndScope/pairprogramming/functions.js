// Question 1
function maxOfTwoNumbers(num1, num2) {
  if (num1 > num2) {
    return num1;
  } else {
    return num2;
  }
};

// Question 2
function maxOfThree(num1, num2, num3) {
  return Math.max(num1,num2,num3);
}

// Question 3
function isCharacterAVowel(myChar) {

  var vowelArray = ["a","e","i","o","u"];

  if (vowelArray.indexOf(myChar) === -1 ){
    return false;
  } else {
    return true;
  }
};

// Question 4


function sumArray(myArray) {

  var sum = 0;

  for (var i = 0; i < myArray.length; i++ ) {
      sum += myArray[i];
  }

  return sum;
 
};


// Question 4

function multiplyArray(myArray) {

  var mult = 1;

  for (var i = 0; i < myArray.length; i++ ) {
      mult *= myArray[i];
  }

  return mult;

}

// Question 5
var numberOfArguments = function(x,y,z){

  return arguments.length;
    
}



// Question 6
var reverseString = function (s){
  return s.split("").reverse().join(""); 
};


// Question 7
function findLongestWord () {
  
}


// Question 8
function filterLongWords (array, i) {
    
  return array.filter(function(val, index, array) {
    return val.length > i;
  });

}


// Bonus 1
//??????


// Bonus 2
function charactersOccurencesCount() {
  
}

