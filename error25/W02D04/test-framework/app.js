

function describe(description, callback){

	console.log('%c' + description, 'color: blue');
	callback();
}

function it(description,callback){

	console.log('%c' + description, 'color: blue');
	callback();
}

function expect(parameter){

	function log(result){
      if(!!result){
      	console.log('%c' + "pass", 'color: green');
      } else {
      	console.log('%c' + "fail", 'color: red');
      }
	};

	return {
		toBe: function(condition){
			return log(parameter === condition);
		
		},

		toContain: function(condition){
			return log(parameter.indexOf(condition)!== -1)

		}
	}
};

var house = {
	rooms: ['living','lounge','bedroom','bedroom','bathroom'],
};


describe("The House", function(){
	it("has has a living room", function(){

		expect(house.rooms).toContain('living room');
	});
	it("has a kitchen", function(){
    expect(house.rooms).toContain('kitchen');
	});
	it("has a bathroom", function(){
    expect(house.rooms).toContain('bathroom');
	});
  });



