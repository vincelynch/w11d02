/**
 * Arrays
 * Most of your answers should be stored in variables called q1, q2 etc.. and the variables printed to the console.

 	(i.e) console.log("Question 1" + q1)
 */

/**
 * Question 1
 * Create an array of image sources. Use image1.png, image2.png, and image3.png as the array values.
 */

// Your code here
var imageSources = new Array('image1.png','image2.png','image3.png');

// the answer was.

var imageArray = ['image1.png','image2.png','image3.png'];


/**
 * Question 2
 * Using the array from Question 1, store the first element of the array in variable q2.
 */

// Your code here

var q2 = imageArray[0];



/**
 * Question 3
 * Get the length of the first array (number of elements in the array) and store it in variable q3
 */

// Your code here
var q3 = imageSources[0].length;

// answer was

var q3 = imageArray[0].length

/**
 * Question 4
 *  Using the array from Question 1, store the last element of the array in variable q4. Hint: How can we get the number of elements in the array?
 */


// Your code here
var getLastE = imageSources.length 
getLastE = getLastE - 1;

var q4 = imageSources[getLastE];


// solution

var getLast = imageArray.length - 1;
var q4 = imageArray[getLast];

// another solution - the solution needs more lines because it damages the array

var q4 = imageArray.pop();

// another solution - worst the solution

var q4 = imageArray[2];

// the real solution

var q4 = imageArray[imageArray.length-1];



// ____________________________________________________________________________

/**
 * Arrays + Iteration
 * Most of your answers should be stored in variables called q1, q2 etc.. and the variables printed to the console.

 	(i.e) console.log("Question 1" + q1)
 */

 q1 = imageSources;
 ["image1.png", "image2.png", "image3.png"]
 console.log("Question 1" + q1)
 VM6300:2 Question 1image1.png,image2.png,image3.png
 undefined


/**
 * Question 1
 * Create an array of numbers using 1,2,3, and 4 as values. Use forEach to increase each value by 1 and store the value back in the array.
The end result should be an array of numbers with values 2,3,4,5
 */

// Your code here


// a solution

var arr = [1, 2, 3, 4];
for (i= 0; i<arr.length; i++){
arr[i] +=1;

}
console.log(arr)


// another solution

var arr = [1,2,3,4];
for(var i=0;i<arr.length;i++) {

  arr[i] +=1;
}
console.log(arr);


// the real answer

arr.forEach(function(item, index, array) {

array[index] = item +1;

});

// for each will always start at array index zero.





   //  arr.forEach(function(elem) {
   //    elem = elem + 1;
 
   // console.log(elem);
   // // var arrNnd = new array[null];
   // var arrNnd
   // arrNnd.add(elem);


   //  });





/**
 * Question 2
 * Using the array from Question 1, find the average of the numbers in the array (average=sum of all numbers/number of numbers). Store the average in q2.
 */


// Your code here

 
for (i= 0; i<arr.length; i++){
arr[i] + arr[i + 1];

}

// again

var stringText = arr.toString()
>stringText.join(“ + “)


// again


arr.forEach(function(item, index, array) {

array[index] = item +1;

});


// Solution

var sum = 0;
q1.forEach(function(number) {
  sum = sum + number;

});

Var q2 = sum / q1.length;

// another solution

var sum = 0;
for(var i=0;i<q1.length;i++) {
sum +=q1[i];

}
var q2 = sum / q1.length;



// another solution

var sumOfArray = q1.reduce(function(a,b) {
return a+b;
});
var arrayLength = q1.length;
var q2 = sumOfArray / arrayLength;


// notes about the above - Good Solution
var q1 = [1,2,3,4];
var sum = q1.reduce(function(prev, currentItem) {
 return prev + currentItem;
},0) / q1.length;

console.log(sum)




