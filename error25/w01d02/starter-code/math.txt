# Convert the words to numbers and symbols (use Command-D for multi-select)
# For example, the first line below should be changed to: 1 + 1 = 2
# Hint: use multi-select
1 plus 1 equals 2
1 plus 2 equals 3
1 plus 3 equals 4
2 plus 3 equals 5
4 plus 1 equals 5
4 minus 2 equals 2
4 minus 3 equals 1
5 minus 3 equals 2
6 minus 4 equals 2
2 times 2 equals 4
2 times 3 equals 6
3 times 2 equals 6
4 divided by 2 equals 2
6 divided by 2 equals 3
6 divided by 3 equals 2
