function AudioTrack(title, artist, album, trackLength, genre, releaseDate) {
	this.title = title;
	this.artist = artist;
	this.album = album;
	this.trackLength = trackLength;
	this.genre = genre;
	this.releaseDate = releaseDate;
	this.currentPosition = 0;

};



    AudioTrack.prototype.skipForwards = function() {
        if (this.currentPosition < (this.trackLength - 10)) {
		return this.currentPosition += 10;
        } else {
          this.currentPosition = this.trackLength;
        }
	};
    AudioTrack.prototype.skipBackwards = function() {
        if (this.currentPosition > 10) {
		return this.currentPosition -= 10;
        } else {
        return this.currentPosition = 0;  
	};

    AudioTrack.prototype.reset = function() {
    this.currentPosition = 0;
	};

    AudioTrack.prototype.timeRemaining = function() {
    return (this.trackLength - this.currentPosition);
	};

}



var thriller = new AudioTrack("Thriller", "Michael Jackson", "Thriller", 406, "Disco Funk", "January 23rd 1984" );
var billieJean = new AudioTrack("Billie Jean", "Michael Jackson", "Billie Jean", 295, "Disco Funk","January 2, 1982")

AudioTrack.prototype.recordCompany = "Epic Label";
billieJean.recordCompany = "Stackz";

/*
you can also use 
delete billieJean.recordCompany
to delete a property.*/

