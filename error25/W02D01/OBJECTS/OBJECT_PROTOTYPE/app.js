/*console.log("connected");
title (string)
artist (string)
album (string)
trackLength (number in seconds)
genre (string)
release date ("string")*/


function AudioTrack(title, artist, album, trackLength, genre, releasedate) {
	this.title = title;
	this.artist = artist;
	this.album = album;
	this.trackLength = trackLength;
	this.genre = genre;
	this.releasedate = releasedate;
	this.currentPosition = 0;

this.skipForwards = function() {
        if (this.currentPosition < (this.trackLength - 10)) {
		return this.currentPosition += 10;
        }
	}
this.skipBackwards = function() {
        if (this.currentPosition > 10) {
		return this.currentPosition -= 10;
        } else {
        return this.currentPosition = 0;  
	}

this.reset = function() {
    this.currentPosition = 0
	}


this.timeRemaining = function() {
    return this.trackLength - this.currentPosition
	}

}
}

var track1 = new AudioTrack("What do you mean", "Justin Bieber", "All about me", 180, "POP", "2016");
var track2 = new AudioTrack("Thiller", "Michael Jackson", "Thiller", 406, "Disco Funk", "January 23, 1984");
