function AudioTrack(title, artist, album, trackLength, genre, releaseDate) {
	this.title = title;
	this.artist = artist;
	this.album = album;
	this.trackLength = trackLength;
	this.genre = genre;
	this.releaseDate = releaseDate;


    this.skipForwards = function() {
        if (this.currentPosition < (this.trackLength - 10)) {
		return this.currentPosition += 10;
        }
	}
     this.skipBackwards = function() {
        if (this.currentPosition > 10) {
		return this.currentPosition -= 10;
        } else {
        return this.currentPosition = 0;  
	}

    this.reset = function() {
    this.currentPosition = 0
	}


   this.timeRemaining = function() {
    return this.trackLength - this.currentPosition
	}

}

}