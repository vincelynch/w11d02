console.log("connected")


function Monkey(name, species) {
	this.name = name;
	this.species = species;
	this.foodsEaten = [];
}

Monkey.prototype.eatSomething = function(food) {
	this.foodsEaten.push(food);
}

Monkey.prototype.introduce = function() {
    return ("Hi my name is; " + this.name + " I am from the " + this.species + " species, and my favourite food is " + this.foodsEaten);
}



var monkey1 = new Monkey("Tony", "Tamarin");
monkey1.eatSomething("Banana");

var monkey2 = new Monkey("George", "Baboon");
monkey2.eatSomething("Olives");

var monkey3 = new Monkey("Harry", "Spider Monkey");
monkey3.eatSomething("Mangos");

var monkey4 = new Monkey("Robert", "Oranotang");
monkey4.eatSomething("Leaves");

var monkey5 = new Monkey("Barry", "Tamarin");
monkey5.eatSomething("Nuts");


console.log(monkey1.introduce());
console.log(monkey2.introduce());
console.log(monkey3.introduce());
console.log(monkey4.introduce());
console.log(monkey5.introduce());


Monkey.prototype.order = "Primate";

console.log("Has primate being assigned?");



console.log(monkey1.order);
console.log(monkey2.order);
console.log(monkey3.order);
console.log(monkey4.order);
console.log(monkey5.order);


// change one of the monkeys "order" to "near human"

monkey1.order = "Near human";
console.log(monkey1.order)

// change monkey names
var reName = function(){
monkey1.name = prompt("What would you like to re-name: " + "monkey1");
monkey2.name = prompt("What would you like to re-name: " + "monkey2");
monkey3.name = prompt("What would you like to re-name: " + "monkey3");
monkey4.name = prompt("What would you like to re-name: " + "monkey4");
monkey5.name = prompt("What would you like to re-name: " + "monkey5");

console.log(monkey1.name);
console.log(monkey2.name);
console.log(monkey3.name);
console.log(monkey4.name);
console.log(monkey5.name);
}

/*

function NewMonkeySeq() {
var variablenameformonkey = prompt("What is the VariableName:", "");
var name = prompt("What is the name of the monkey?:", "");
var species = prompt("And what is his species?", "");
var WhatDoesHeEat = prompt("What does he eat?")

return variablenameformonkey = new Monkey(name, species);
variablenameformonkey.eatSomething(WhatDoesHeEat);
}
*/