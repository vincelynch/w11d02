// adds audio element as variable
/*

var audio = document.getElementsbyTagName('audio')[0];
*/
/*
built in functions for audio in javascript as OBJECTs
audio.play()
audio.pause()
audio.currentTime // will return the current time of where thetrack is at
so if you 
audio.currentTime = 0
then you set the current time back to the start.

/ can tell us, what audio type is supported on their computer
audio.canPlayType("audio/ogg")
audio.canPlayType("audio/mp3")


So now we can take the default controls off. and instead create our own buttons and control it using javascript
*/

document.addEventListener("DOMContentLoaded", function(event){




 var audio = document.getElementsByTagName("audio")[0];
 var PlayPause = document.getElementById("PlayPause");
 var timestamp = document.getElementById("seconds");

PlayPause.addEventListener("click", function(){

if (this.className === "paused") {
	 audio.play();
     this.className = "playing";

} else {

 audio.play();
     this.className = "paused";
    }

 timestamp.innerHTML = audio.currentTime;

  });


});

